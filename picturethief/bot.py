"""
Picturethief by /u/trollsalot1234
 
searches SUBREDDITS and x-posts Imgur links

some code ganked from: https://gist.github.com/avidw/9438841
and: https://github.com/pandanomic/SpursGifs_xposterbot
"""
from __future__ import print_function # to print to stderr
import praw, bmemcached, time, os     # for all bots
from requests import HTTPError        # to escape ban errors
import sys                            # for all errors
import re                             # if you want to use regex
import textwrap                       # if you want to post multiline comments
import logging                        # if you want to use logs
import argparse                       # argument parsing
import pickle                         # dump list and dict to file
from bs4 import BeautifulSoup         # html parsing
import urllib2                        # open urls
import xml.sax.saxutils as saxutils   # for unescaping (because beautifulsoup doesnt do &amp as it is a bitch)

# Color class, used for colors in terminal
class Color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

# subreddits to look in for content to x-post
SUBREDDIT_DICT = {
'Can' : ['Canada', 'Acadie', 'CanadianMaritimes', 'FrancoOntarien', 'TaGueule'], 
'AB' : ['Alberta', 'Calgary', 'Edmonton', 'FortMac', 'Lethbridge', 'MedicineHat', 'PeaceCountry', 'RedDeer', 'SherwoodPark', 'TownOfBanff'], 
'BC' : ['BritishColumbia', 'Abbotsford', 'Armstrong', 'CampbellRiver', 'Chilliwack', 'ComoxValley', 'CrestonBC', 'Coquitlam', 'FraserValley', 
'GulfIslands', 'Kamloops', 'Kelowna','Kootenays', 'MapleRidge', 'Nanaimo', 'OceanFalls', 'Okanagan', 'PeaceCountry', 'Pemberton', 'Penticton', 
'SouthIsland', 'Squamish', 'Tofino', 'Vancouver', 'VancouverIsland', 'Vernon', 'VictoriaBC', 'Whistler'], 
'MB' : ['Manitoba', 'BrandonManitoba', 'Carberry', 'FlinFlon', 'SwanRiver', 'Winnipeg'], 
'NB' : ['NewBrunswickCanada', 'Fredericton', 'Moncton', 'SaintJohnNB'], 
'NL' : ['Newfoundland', 'ConceptionBaySouth', 'LabCity', 'StJohnsNL'], 
'NS' : ['NovaScotia', 'AnnapolisValley', 'CapeBreton', 'CityofLakes', 'DartmouthNovaScotia', 'Fairmount', 'Halifax', 'NewGlasgow', 'Pictou', 'Sackvegas'],
'ONT' : ['Ontario', '905', 'Barrie', 'Brampton', 'Brantford', 'BurlingtonON', 'ChathamKent', 'Durham', 'Guelph', 'Hamilton', 'KingstonOntario', 
'Kitchener', 'LondonOntario', 'Markham', 'Milton', 'Mississauga', 'Newmarket', 'Niagara', 'NorthBay', 'Oakville', 'Oshawa', 'Ottawa', 'ParrySound', 
'Peterborough', 'Pickering', 'porthope', 'Sarnia', 'SaultSteMarie', 'Scarborough', 'SouthWesternOntario', 'StratfordOntario', 'StThomasOntario', 
'Sudbury','ThunderBay', 'Toronto', 'Vaughan', 'Waterloo', 'WindsorOntario', 'YrkRegion'], 
'PE' : ['PEI', 'Charlottetown'], 
'QC' : ['Quebec', 'Abitibi', 'Gatineau', 'Laval', 'Longueuil', 'Matane', 'Mistissini', 'Montreal', 'QuebecCity', 'QuebecLevis',
'RiviereduLoup', 'Saguenay', 'SaintHyacinthe', 'Sherbrooke', 'TroisRivieres', 'WestIslandMtl'],
'SK' : ['Saskatchewan', 'Delisle', 'MooseJaw', 'Regina', 'Saskatoon', 'Weyburn', 'Yorkton'], 
'NT' : ['NWT', 'Nunavut', 'Yukon', 'Iqualuit', 'Yellowknife']
}

POST_SUBREDDITS = ['canadainpictures', 'picturethief']    # subreddits to x-post to
MIN_SCORE = 10                                            # minimum karma a submission must have to be x-posted
POSTSUB_LIMIT = 1000                                      # how deep to look into POST_SUBREDDITS for x-posts
POST_LIMIT = 100                                          # max number of submissions to pull
DEFAULT_LOG_VIEW = 30                                     # default log view set to WARNING


class PictureCache:

  def __init__(self):
    CACHE_FILE = "Picturethief_DB"                        # Cache file if not using mc
    self.__use_mc__ = False
    if os.environ.get('MEMCACHEDCLOUD_SERVERS', None):
      self.__use_mc__ = True
      self.__my_cache__ = bmemcached.Client((os.environ['MEMCACHEDCLOUD_SERVERS'],), 
                                 os.environ['MEMCACHEDCLOUD_USERNAME'],
                                 os.environ['MEMCACHEDCLOUD_PASSWORD'])
    if not self.__use_mc__:
      self.__my_cache__ = []
      log(logging.DEBUG, "Loading cache", Color.BLUE)
      if os.path.isfile(CACHE_FILE):
        with open(CACHE_FILE, 'r+') as db_file_load:
          self.__my_cache__ = pickle.load(db_file_load)
      log(logging.DEBUG,'--Cache size: ' + str(len(self.__my_cache__)), Color.BLUE)


  def check_cache(self, input_key):
    """
    check cache for string input_key
    """
    log(logging.DEBUG, 'Checking cache for ' + str(input_key), Color.BLUE)

    if self.__use_mc__:
      obj = self.__my_cache__.get(str(input_key))
      if not obj or obj != "True":
        return False
      else:
        return True
    else:
      if input_key in self.__my_cache__:
        return True
      else:
        return False


  def cache_key(self, input_key):
    """
    add sting input_key to cache
    """
    log(logging.DEBUG, 'Caching ' + str(input_key), Color.BLUE)

    if self.__use_mc__:
      self.__my_cache__.set(str(input_key), "True")
      assert str(self.__my_cache__.get(str(input_key))) == "True"
    else:
      self.__my_cache__.append(input_key)

    log(logging.DEBUG,'--Cached ' + str(input_key), Color.BLUE)


  def cache_remove_key(self, input_key):
    """
    remove string input_key from cache
    """
    log(logging.DEBUG,"Removing " + input_key + " from cache", Color.BLUE)

    if self.__use_mc__:
      self.__my_cache__.delete(str(input_key))
    else:
      self.__my_cache__.remove(input_key)

    log(logging.DEBUG,'--Removed ' + str(input_submission.id), Color.BLUE)


#---------------------Functions---------------------#
def config_logging(loglevel):
  """
  If there's a color argument, it'll stick that in first
  loglevel: 1 - DEBUG, 2 - INFO, 3 - WARNING, 4 - ERROR, 5 - CRITICAL
  """
  try:
    log_level = int(loglevel) * 10
  except:
    log_level = getattr(logging, loglevel.upper(), DEFAULT_LOG_VIEW)

  logging.basicConfig(
    stream=sys.stdout,
    level=log_level,
    format='"%(asctime)s %(levelname)8s %(name)s - %(message)s"',
    datefmt='%H:%M:%S'
  )


def log(level, message, *colorargs):
  """
  If there's a color argument, it'll stick that in first
  level: 10 - DEBUG, 20 - INFO, 30 - WARNING, 40 - ERROR, 50 - CRITICAL
  """
  options = {
    logging.DEBUG : logging.debug, 
    logging.INFO : logging.info, 
    logging.WARNING : logging.warning, 
    logging.ERROR : logging.error, 
    logging.CRITICAL : logging.critical
  }

  if len(colorargs) > 0:
    message = colorargs[0] + message + Color.END

  try:
    options[level](message)

  except KeyError as err:
      log(logging.ERROR,"KeyError in log method.  Level must be between 1 and 5", Color.RED)


def exit_bot():
  """
  Function to exit the bot
  """
  log(logging.DEBUG, "Exiting Bot",Color.BLUE)
  sys.exit()


def get_login_info():
  """
  Returns ["username", "password"] for reddit login.
  (or exits if invalid info provided)
  """
  log(logging.DEBUG, "Getting login info",Color.BLUE)

  if args.rusername and args.rpassword:
    return [args.rusername, args.rpassword]
  elif "REDDIT_USERNAME" in os.environ and "REDDIT_PASSWORD" in os.environ:
    return [os.environ['REDDIT_USERNAME'], os.environ['REDDIT_PASSWORD']]

  log(logging.ERROR, "Invalid Login Credentials Provided", Color.RED)
  exit_bot()


def get_imgur_id(s):
  """
  accepts submission s and filters out the imgur ID from s.url
  will leave us with either a clean imgur id eg: YHJ0HFC
  or an album id eg: a/dB20g or IwFPXck,zJZp9HJ,SIFf8n0
  """
  log(logging.DEBUG, "Getting Ingur ID",Color.BLUE)

  junk, temp_id = s.url.encode("utf-8").split(".com/",1)
  if "." in temp_id:
    temp_id, junk = temp_id.split(".",1)
  if "/" in temp_id:
    if "#" in temp_id:
      temp_id, junk = temp_id.split("#",1)
    temp_id = temp_id.replace("/new","")
    temp_id = temp_id.replace("gallery/","")

  log(logging.DEBUG, "ID found: " + temp_id,Color.BLUE)
  return temp_id


def get_resolution(imgur_id):
  """
  return the resolution of an imgur image based on its imgur_id ( "widthxheight" ) 
  or "Album" if its an album
  """
  if "/" in imgur_id or "," in imgur_id:
    return "Album"

  imgurlink = urllib2.urlopen("http://imgur.com/" + imgur_id)
  content = imgurlink.read()

  soup = BeautifulSoup(content)

  #  <meta content="2688" name="twitter:image:width"/>
  #  <meta content="1520" name="twitter:image:height"/>

  width= soup.findAll(attrs={"name":"twitter:image:width"})[0]['content'].encode('utf-8')
  height= soup.findAll(attrs={"name":"twitter:image:height"})[0]['content'].encode('utf-8')

  return width + "x" + height


#---------------------MAIN---------------------#
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description='This is the Picturethief bot by /u/trollsalot1234')
  parser.add_argument('-l','--log', help='Log level to display', required = False)
  parser.add_argument('-ruser', '--rusername', help='Reddit username', required = False)
  parser.add_argument('-rpass', '--rpassword', help='Reddit password', required = False)
  args = parser.parse_args()

  # Configure logging
  if args.log:
    config_logging(args.log)
  else:
    config_logging(DEFAULT_LOG_VIEW)

  # Log in to reddit
  reddit = praw.Reddit("Picturethief 1.0.0 by /u/trollsalot1234")
  try:
    credentials = get_login_info()
    reddit.login(credentials[0], credentials[1])
  except praw.errors:
    log(logging.ERROR,"LOGIN FAILURE", Color.RED)
    exit_bot()

  # Set up DB
  db = PictureCache()

  # Add submissions from POST_SUBREDDITS into DB so they are not reposted
  for post_sub in POST_SUBREDDITS:
    log(logging.INFO, "Scanning for new posts in " + post_sub + " and updating DB", Color.GREEN)

    submissions = reddit.get_subreddit(post_sub).get_new(limit=POSTSUB_LIMIT)

    for s in submissions:

      if "imgur.com/" not in s.url: #if its not an imgur link skip it
        continue

      imgur_id = get_imgur_id(s)

      if db.check_cache(imgur_id): #if imgur id is in the DB skip it
        continue

      db.cache_key(imgur_id)

  # Begin x-posting
  log(logging.INFO, "Searching for new things to x-post",Color.GREEN)

  for province, subreddits in SUBREDDIT_DICT.iteritems():
    log(logging.INFO, "--in " + province, Color.GREEN)
    submissions = reddit.get_subreddit('+'.join(subreddits)).get_new(limit=POST_LIMIT)

    for s in submissions:

      cid = str(s.id)

      if "imgur.com/" not in s.url: #if its not an imgur link skip it
        continue
      if s.score < MIN_SCORE: #if the karma score isnt high enough skip it
        continue
      if db.check_cache(cid): #if this post has been previously x-posted skip it
        continue
      
      imgur_id = get_imgur_id(s)

      if db.check_cache(imgur_id): #if this imgur link has been previously x-posted skip it
        continue

      try:
        my_post = "[X-post: /r/{!s}] {!s}".format(s.subreddit.display_name, saxutils.unescape(s.title.encode("utf-8")))
        my_flair = "[{!s}][{!s}]".format(get_resolution(imgur_id), province)
        my_comment = (
"""
This picture was stolen from /u/{!s} who originally posted it [here]({!s}) over in /r/{!s} 


* I am a bot. If there's an issue with something I did please message /u/trollsalot1234
"""
       ).format(s.author.name, s.permalink.encode("utf-8"), s.subreddit.display_name)

        for post_sub in POST_SUBREDDITS:
          new_post = reddit.submit(post_sub, my_post, url=s.url)
          new_post.set_flair(flair_text=my_flair)
          new_post.add_comment(my_comment)
          log(logging.INFO, "Posted: " + my_post + " to /r/" + post_sub, Color.YELLOW)
        
        db.cache_key(cid)
        db.cache_key(imgur_id)
        
      # If you are banned from a subreddit, reddit throws a 403 instead of a helpful message :/
      except HTTPError as err:
        log(logging.ERROR,"Probably banned from /r/" + str(comment.subreddit), Color.RED)
      # This one is pretty rare, since PRAW controls the rate automatically, but just in case
      except praw.errors.RateLimitExceeded as err:
        log(logging.ERROR,"Rate Limit Exceeded:\n" + str(err) + " when x-posting", Color.RED)
        time.sleep(err.sleep_time)
      # If the post has already been submitted to the sub
      except praw.errors.AlreadySubmitted as err:
        log(logging.ERROR,"AlreadySubmitted error when x-posting.", Color.RED)      
        continue
      # Something happened and Im pretty lazy so just skip it and go to the next one (probably a deleted author)
      except (AttributeError, TypeError) as err:
        log(logging.ERROR,"Attribute or Type error when x-posting.", Color.RED)
        continue

  log(logging.INFO,"Completed run",Color.GREEN)